import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { EnrollmentService } from '../enrollment.service';


@Component({
  selector: 'app-tdform',
  templateUrl: './tdform.component.html',
  styleUrls: ['./tdform.component.css']
})
export class TDformComponent implements OnInit {

  public topics = ["Angular","ABC","CDA"];
  userModel = new User('','rob@gmail.com',5654584574,'','morning',true);
  topicHasError = true;

  constructor(private _enrollmentService: EnrollmentService) { }

  ngOnInit() {
  }

  validateTopic(value){
    if(value == 'default'){
      this.topicHasError = true;
    }
    else{
      this.topicHasError = false;
    }
  }
  
  // onSubmit(){
  //   // console.log(this.userModel);
  //   this._enrollmentService.enroll(this.userModel)
  //     .subscribe(
  //       data => console.log('success',data),
  //       error => console.error('error',error)
  //     )
  // }

}


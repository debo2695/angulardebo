import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  // templateUrl: './employee-list.component.html',
  template: `
    <h2> Employee List </h2>
    <ul *ngFor = "let employe of employee">
      <li>{{employe.name}}</li>
    </ul>
  `,
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  public employee = [];
  
  constructor(private employeeService: EmployeeService) { }


  ngOnInit() {         
   this.employeeService.getEmployees()
        .subscribe(data => this.employee = data);
  }


}



import { AbstractControl } from "@angular/forms";

export function PasswordValidator(control: AbstractControl):{[key: string]:boolean} | null{
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');
    // if(password && confirmPassword ) {
    // return password && confirmPassword && password.value != confirmPassword.value ? 
    // { 'misMatch' : true } : 
    // null;
    // }
    if((password && confirmPassword) && (password.value != confirmPassword.value)){
        return { 'misMatch' : true }
    } else{
        return null
    }
} 


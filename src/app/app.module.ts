import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{ FormsModule }  from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { ExampleComponent } from './example/example.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { DepartmentlistComponent } from './departmentlist/departmentlist.component';
import { DepartmentdetailComponent } from './departmentdetail/departmentdetail.component';
import { EmployeeService } from './employee.service';
import{ HttpClientModule } from '@angular/common/http';
import { TDformComponent } from './tdform/tdform.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { ReactiveFormsModule} from '@angular/forms'; 

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    EmployeeListComponent,
    EmployeeDetailComponent,
    ExampleComponent,
    PagenotfoundComponent,
    DepartmentlistComponent,
    DepartmentdetailComponent,
    TDformComponent,
    ReactiveFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }

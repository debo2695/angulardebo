import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-departmentdetail',
  // templateUrl: './departmentdetail.component.html',
  template:`
  <h3>You have selected id = {{DepartmentId}}</h3>
  <a (click) = "goPrevious()"> GoPrevious </a>
  <a (click) = "goNext()"> GoNext </a>
  <a (click) = "gotoDepartments()"> Back </a>
  `,
  styleUrls: ['./departmentdetail.component.css']
})
export class DepartmentdetailComponent implements OnInit {

  public DepartmentId;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  //   let id = parseInt(this.route.snapshot.paramMap.get('id'));
  //   this.DepartmentId = id;
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id =parseInt(params.get('id'));
      this.DepartmentId = id;

    });
  }

  goPrevious(){
    let PreviousId = this.DepartmentId - 1;
    this.router.navigate(['/department',PreviousId]);
  }

  goNext(){
    let NextId = this.DepartmentId + 1;
    this.router.navigate(['/department',NextId]);
  }

  gotoDepartments(){
    let selectedId = this.DepartmentId ? this.DepartmentId:null;
    this.router.navigate(["/department",{id: selectedId}]);
  }

}

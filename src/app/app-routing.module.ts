import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { EmployeeListComponent } from './employee-list/employee-list.component';
import { ExampleComponent } from './example/example.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { DepartmentlistComponent } from './departmentlist/departmentlist.component';
import { DepartmentdetailComponent } from './departmentdetail/departmentdetail.component';
// import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';

const routes: Routes = [
  // {
  //   path: "",
  //   redirectTo: "/list", 
  //   pathMatch: "full"
  // },
  {
    path: "list",
    component: ExampleComponent
  },
  {
    path: "detail",
    component: ExampleComponent
  },
  {
    path: "department",
    component: DepartmentlistComponent
  },
  {
    path:"department/:id",
    component: DepartmentdetailComponent
  },
  {
    path: "**",
    component: PagenotfoundComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

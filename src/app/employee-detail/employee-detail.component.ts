import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-detail',
  // templateUrl: './employee-detail.component.html',
  template: `
  <h2>Employee detail</h2>
  <ul *ngFor = "let employe of employee">
      <li>{{employe.id}} {{employe.name}} - {{employe.age}}</li>
  </ul>

  `,
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  public employee = [];
  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {         
    this.employeeService.getEmployees()
         .subscribe(data => this.employee = data);
   } 

}

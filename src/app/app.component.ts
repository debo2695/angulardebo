import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularDev';
  desc = 'This is the new description about the project!';
  public firstname = "devyani";
  public message = "";

 
}

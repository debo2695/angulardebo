import { Component, OnInit } from '@angular/core';
import { EnrollmentService } from '../enrollment.service';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.css']
})
export class ExampleComponent implements OnInit {
  post ='';
  constructor(private enrollmentservice: EnrollmentService) { }

  ngOnInit() {
    this.enrollmentservice.enroll().subscribe(
      data=>this.post = data
    )
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-test',
  template: `
  <h3>Welcome {{name}} {{name.length}}</h3><br>
  {{2+2}}  {{name.toUpperCase()}}<br>
  {{"welcome" + name}}<br>
  {{site}}
  {{greetUser()}} <br>
  Login Name: <input [id]= "Myid" type="text" value="hiii"><br><br>
  Login Name2: <input [disabled] ="isdisabled" id= "{{Myid}}" type="text" value="hiii"><br><br>
  Password: <input type="password"><br>
   <input type="submit"><br>
   <h2 [class]="Special"> New Words</h2>
   <h2 class="text-special" [class]="SuccessClass"> New Words</h2>
   <h2 [class.text-danger] = "hasError"> Helllllllo11</h2>
   <h2 [ngClass] = "Messages"> Helllllllo</h2>
   <h2 [style.color]="hasError ? 'red' : 'orange'"> Style Binding </h2>
   <h2 [style.color]="Highlight"> Style Binding 2 </h2>
   <h2 [ngStyle]="StyleBind"> Style Binding 3 </h2>
   <button (click)="onClick()"> Greet </button> {{greeting}}
   <button (click)="greeting = 'welcome here 2'"> Greet 2 </button> &nbsp;<br><br>
   <input #logevent type="text">  &nbsp;
   <button (click)="Logevent(logevent.value)"> Log </button><br><br>
   <input type="text" [(ngModel)]="name1">{{name1}}
   
    <h2 *ngIf="DisplayName; else ElsePart"> 
      It's in if block!
    </h2>
    <ng-template #ElsePart>
      <h2> 
        It's in else block
      </h2>
    </ng-template>
    

    <div *ngIf="DisplayName2; then ThenBlock2; else ElsePart2">
    </div>  
    <ng-template #ThenBlock2>
      <h2> You are once again in the If block </h2>
    </ng-template> 
    <ng-template #ElsePart2>
      <h2> You are now in the else part </h2>
    </ng-template>  


    <div [ngSwitch]="color1">
      <div *ngSwitchCase="'red'"> You picked red color </div>
      <div *ngSwitchCase="'yellow'"> You picked yellow color </div>
      <div *ngSwitchCase="'violet'"> You picked violet color </div>
      <div *ngSwitchDefault>You have not selected any color </div>
    </div>


     <div *ngFor = "let color of colors; index as i">
       <h2> {{i}} {{color}} </h2>
    </div>
    <hr>
    <div *ngFor = "let color of colors; last as l">
       <h2> {{l}}  {{color}} </h2>
    </div>
    <hr>
    <div *ngFor = "let color of colors; first as f">
        <h2> {{f}} {{color}} </h2> 
    </div>
    <hr>
    <div *ngFor = "let color of colors; odd as o">
        <h2> {{o}} {{color}} </h2> 
    </div>
    <hr>
    <div *ngFor = "let color of colors; even as e">
        <h2> {{e}} {{color}} </h2> 
    </div>
    <hr>

    <h2> {{"Hello123" + name123}} </h2>
    <button (click)="fireEvent()">Emit</button> 

    <h2> {{example1 | titlecase }} </h2>
    <h2> {{example1 | slice:3:5 }} </h2>
    <h2> {{example2 | json}} </h2>
    <h2> {{0.56 | percent}} </h2>
    <h2> {{25 | currency:'INR':'code'}} </h2>
    <h2> {{date}} <h2>
    <h5> {{date | date: 'short'}} <h5>
    <h5> {{date | date: 'shortDate'}} <h5>
    <h5> {{date | date: 'shortTime'}} <h5>


  `,
  // template: '<div>Inline Template</div>',
  // templateUrl: './test.component.html',
  // styleUrls: ['./test.component.css']
  styles: [`
 
  .text-success{
    color: green;
  }
  .text-danger{
    color:red;
  }
  .text-special{
    font-style: italic; 
  }
  `]
})
export class TestComponent implements OnInit {
  // public name = ["debo","usman"];
  public name = "debo";
  public site = window.location.href;
  public Myid = "myid12";
  public isdisabled =false;
  public SuccessClass = "text-success"
  public Special = "text-special"
  public hasError = true;
  public Highlight = "green";
  public isSpecial = true;
  public Messages = {
    "text-danger": this.hasError,
    "text-success": !this.hasError,
    "text-special": this.isSpecial
  }
  public StyleBind = {
    color : "yellow",
    fontStyle: "italic"

  }
  public greeting = "";
  public name1="";
  public DisplayName = false;
  public DisplayName2 = true;
  public color1 = "";
  public colors = ["red","yellow","blue","green"];
  @Input('parentData') public name123;
  @Output() public childEvent = new EventEmitter();
  public example1 = "hello world";
  public example2 ={
    "firstname": "debo",
    "lastname": "debo2"
  }
  public date = new Date();

  constructor() { }

  ngOnInit() {
  }

  greetUser() {
    return "hellooo "+ this.name;
  }

  onClick(){
    console.log('welcome');
    alert('wc');
    this.greeting = "Welcome here";
  }

  Logevent(value){
    console.log(value);
  }

  fireEvent(){
    this.childEvent.emit('hey get lost');
  }
}

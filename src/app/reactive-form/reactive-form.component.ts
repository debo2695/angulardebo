import { Component, OnInit } from '@angular/core';
// import { FormControl, FormGroup } from '@angular/forms';
import{FormBuilder, Validators, FormGroup, FormArray} from '@angular/forms';
import { forbiddenNameValidator } from '../shared/user-name.validator';
import { PasswordValidator } from '../shared/password.validator';


@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  registrationForm: FormGroup;

  // registrationForm = new FormGroup({
  //   userName: new FormControl(''),
  //   password: new FormControl(''),
  //   confirmPassword: new FormControl(''),
  //   address: new FormGroup({
  //     city: new FormControl(''),
  //     state: new FormControl(''),
  //     postalCode: new FormControl('')
  //   }),
    // groupname:new FormGroup({
    //   firstname: new FormControl(''),
    //   lastname: new FormControl(''),
    //   middlename: new FormControl('')
    // })
  // });

  get userName(){
    return this.registrationForm.get('userName');
  }

  get email(){
    return this.registrationForm.get('email');
  }

  get alternateEmails(){
    return this.registrationForm.get('alternateEmails') as FormArray;  
  }

  addAlternateEmail(){
    this.alternateEmails.push(this.fb.control(''));
  }

  constructor(private fb: FormBuilder) { }
  

  ngOnInit() {
    this.registrationForm = this.fb.group({
      // userName: ['',[Validators.required,Validators.minLength(3), forbiddenNameValidator]],
      userName: ['',[Validators.required,Validators.minLength(3), forbiddenNameValidator(/password/)]],
      email: [''],
      subscribe: [false],
      password: [''], 
      confirmPassword: [''],
      address: this.fb.group({
        city: [''],
        state: [''] ,
        postalCode:['']
      }),
      alternateEmails: this.fb.array([ ])
    }, {validator: PasswordValidator} );

    // console.log(this.registrationForm.controls);

    this.registrationForm.get('subscribe').valueChanges
      .subscribe(checkedValue =>{ 
        const email = this.registrationForm.get('email');
        if(checkedValue){
           email.setValidators(Validators.required);
        }else{
          email.clearValidators();
        }
        email.updateValueAndValidity();

      });
  }

  register(value){
    console.log(this.registrationForm.controls);
    // alert(JSON.stringify(this.userName.errors.required) ); 
    // console.log(value);
    // console.log('=========');
    // console.log(value.password);
    // if(value.password != value.confirmPassword){
    //   alert('confirm password does not match with the password');
    //   console.log('password does not match with the password');
    // }
    // else{ 
    //   alert('correct password');
    //   console.log('correct password');
    // }
  }

  loadapidata(){
    this.registrationForm.patchValue({
      userName: 'Jensen',
      password: "123",
      confirmPassword: "123",
     

    })
  }

}

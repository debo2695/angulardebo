import { Component, OnInit } from '@angular/core';
import{ Router, ActivatedRoute, ParamMap } from '@angular/router';
 
@Component({
  selector: 'app-departmentlist',
  // templateUrl: './departmentlist.component.html',
  template:`
    <h3>Department list</h3>
    <ul class="items">
      <li (click)="onSelect(department)" [class.selected]="isselected(department)" *ngFor="let department of departments">
        <span class="badge">{{department.id}}</span>{{department.name}}
      </li>  
    </ul>
  `,
  styleUrls: ['./departmentlist.component.css']
})
export class DepartmentlistComponent implements OnInit {

  public selectedId;
  public departments = [
    {"id":1, "name": "debo"},
    {"id":2, "name": "usman"},
    {"id":3, "name": "irshad"},
  ]

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.selectedId = id;
  });
}

  onSelect(department){
    this.router.navigate(["/department",department.id]);
    //  alert("hi") ;
    //  console.log('h');
  }

  isselected(department){ 
    return department.id === this.selectedId; 
  }

}
